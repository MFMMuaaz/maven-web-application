package com.muaz.webapp.functions;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

// This class will create a connection pool
public class connectionPool {
    final static Logger logger = Logger.getLogger(serializer.class);
    private static BasicDataSource ds = new BasicDataSource();
    static {
        ds.setUrl("jdbc:mysql://localhost:3306");
        ds.setUsername("root");
        ds.setPassword("6401430muaz");
        ds.setMinIdle(5);
        ds.setMaxIdle(10);
        ds.setMaxOpenPreparedStatements(100);
        System.out.println("Pool created");
        logger.info("Pool created");
    }
    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
    private connectionPool(){ }
}