package com.muaz.webapp.functions;

import org.apache.log4j.Logger;

import java.io.IOException;

// This class will be used to write the xml payload to a .xml file
public class fileWriter {
    final static Logger logger = Logger.getLogger(fileWriter.class);
    public void WriteToFile(String data, String path) {
        try {
            java.io.FileWriter fw = new java.io.FileWriter(path);
            fw.write(data);
            fw.close();
        } catch (IOException e) {
            logger.error("There was an error.", e);
            System.out.println("This is that: "+e);
        }
        return;
    }
}