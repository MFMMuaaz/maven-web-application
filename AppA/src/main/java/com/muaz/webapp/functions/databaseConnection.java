package com.muaz.webapp.functions;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class databaseConnection {
    final static Logger logger = Logger.getLogger(databaseConnection.class);

    public void connectAndWrite(String XML) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            logger.error("There was an error.", e);
            System.out.println("MySQL JDBC Driver not found !!");
            return;
        }

        String id,first_name,last_name,statcd;
        Connection connection = null;
        try {
            retrieveFromXML XMLvalues = new retrieveFromXML();

            id = XMLvalues.getTagValue(XML,"ID");
            first_name = XMLvalues.getTagValue(XML,"FIRST_NAME");
            last_name = XMLvalues.getTagValue(XML,"LAST_NAME");
            statcd = XMLvalues.getTagValue(XML,"STAT_CD");
            connection = connectionPool.getConnection();

            PreparedStatement ps = connection.prepareStatement("INSERT INTO EMPLOYEE (ID,FIRST_NAME,LAST_NAME,STAT_CD) VALUES (?,?,?,?)");
            ps.setString(1, id);
            ps.setString(2, first_name);
            ps.setString(3, last_name);
            ps.setString(4, statcd);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            return;
        } finally {
            try {
                if (connection != null)
                    connection.close();
                System.out.println("Connection closed !!");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

