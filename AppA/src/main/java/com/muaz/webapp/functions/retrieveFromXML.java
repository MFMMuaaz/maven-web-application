package com.muaz.webapp.functions;

// This will be used to retrieve values from XML
public class retrieveFromXML {
    public String getTagValue(String xml, String tagName) {
        return xml.split("<" + tagName + ">")[1].split("</" + tagName + ">")[0];
    }
}
