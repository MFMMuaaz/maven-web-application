package com.muaz.webapp.configuration;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("application/")
public class applicationConfig extends Application {
}
