package com.muaz.webapp.maincode;

import com.muaz.webapp.functions.XSD_validator;
import com.muaz.webapp.functions.databaseConnection;
import com.muaz.webapp.functions.fileWriter;
import com.muaz.webapp.functions.serializer;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/function")
// This is the class where the program starts
public class mainCode {
    @Path("/validate")
    @POST
    @Consumes({"application/xml"})
    @Produces({"application/xml"})
    // This method will be called when we add "/validate" in the right most of the url.
    public Response validation(String Data) {
        fileWriter FR = new fileWriter();
        FR.WriteToFile(Data,"/home/muaz/Desktop/MVN/AppA/src/main/Resources/students.xml");
        XSD_validator val = new XSD_validator();
        boolean valid = val.validate();
        if (valid){
            databaseConnection db = new databaseConnection();
            db.connectAndWrite(Data);
            return Response.status(200).entity("It is a valid xml.").build();
        }
        else{
            return Response.status(400).entity("Invalid xml.").build();
        }
    }

    @Path("/serialize")
    @POST
    @Consumes({"application/xml"})
    @Produces({"application/json"})
    // This method will be called when we add "/serialize" in the right most of the url.
    public Response serialization(String Data){
        serializer SR = new serializer();
        String serializedData = SR.serialize(Data);
        if (serializedData.equals("ERROR while serializing")){
            return Response.status(400).entity(serializedData).build();
        }
        else{
            return Response.status(200).entity(serializedData).build();
        }
    }
}
